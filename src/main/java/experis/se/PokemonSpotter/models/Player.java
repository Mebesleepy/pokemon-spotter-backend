package experis.se.PokemonSpotter.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/*
* Generic user model with the exception for the spotted pokemon list that is connected to "as the name implies"
* the pokemon that the player has spotted.
* */

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(nullable = false, unique = true)
    public String username;

    @Column(nullable = false)
    public String password;

    @Column
    public int score;

    @OneToMany(mappedBy="player", orphanRemoval = true)
    public List<SpottedPokemon> spottedPokemon = new ArrayList<>();

}
