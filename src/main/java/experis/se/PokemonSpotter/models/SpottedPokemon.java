package experis.se.PokemonSpotter.models;

import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

/*
* Arguably the most important class that holds the most amount of information
* that is used. Straightforward, and connected to a single player.
* */

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class SpottedPokemon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    @Column(nullable = false)
    public int pokemonId;
    @Column(nullable = false)
    public int seen;
    @Column(nullable=false)
    public boolean foundMale;
    @Column(nullable=false)
    public boolean foundFemale;
    @Column(nullable=false)
    public boolean foundShiny;
    @Column(nullable=false)
    public String firstFoundLatitude;
    @Column(nullable = false)
    public String firstFoundLongitude;
    @Column(nullable = false)
    public LocalDateTime firstFoundDate;

    @ManyToOne(fetch = FetchType.LAZY)
    public Player player;

}
