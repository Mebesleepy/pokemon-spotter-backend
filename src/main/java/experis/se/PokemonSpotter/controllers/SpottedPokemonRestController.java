package experis.se.PokemonSpotter.controllers;
import experis.se.PokemonSpotter.models.*;
import experis.se.PokemonSpotter.repositories.PlayerRepository;
import experis.se.PokemonSpotter.repositories.GlobalSeenPokemonRepository;
import experis.se.PokemonSpotter.repositories.SpottedPokemonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;

@RestController
public class SpottedPokemonRestController {

    @Autowired
     private PlayerRepository playerRepository;

    @Autowired
    private SpottedPokemonRepository spottedPokemonRepository;

    @Autowired
    private GlobalSeenPokemonRepository globalSeenPokemonRepository;

    /*
    * Get a "global pokemon" not used in frontend but to see the values in "testing".
    * */
    @GetMapping("/pokemon/{id}")
    public ResponseEntity<GlobalSeenPokemon> getPokemon(@PathVariable Integer id) {

        Optional<GlobalSeenPokemon> globalSeenPokemon = globalSeenPokemonRepository.findByPokemonId(id);

        if(globalSeenPokemon.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(globalSeenPokemon.get(), HttpStatus.OK);
    }


    @GetMapping("/player/all")
    public ResponseEntity<List<Player>> getAllPlayers() {

      List<Player> players = playerRepository.findAll();

      if (players.isEmpty()) {
          return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }

        return new ResponseEntity<>(players, HttpStatus.OK);
    }

    @GetMapping("/player/get")
    public ResponseEntity<Player> getPlayer(@RequestParam String username) {

        System.out.println(username);

        Player player =  playerRepository.getByUsername(username);

        if(player == null) {
            System.out.println("The user " + username + " does not exist");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(player,HttpStatus.OK);
    }

    @PostMapping("/player/create")
    public ResponseEntity<Player> createUser(@RequestBody Player player) {

        Player existingPlayer = playerRepository.getByUsername(player.username);

        if (existingPlayer != null) {
            return new ResponseEntity<>(player, HttpStatus.BAD_REQUEST);
        }

        player = playerRepository.save(player);

        return new ResponseEntity<>(player,HttpStatus.CREATED);
    }

    @GetMapping("/player/rank")
    public ResponseEntity<Integer> getPlayerRank(@RequestParam String username) {

        List<Player> allPlayers = playerRepository.findAll();

        allPlayers.sort(Comparator.comparingInt(player -> player.score));

        Collections.reverse(allPlayers);

        int playerRank = 0;

        for (Player player : allPlayers) {
            playerRank++;

            if (player.username.equals(username)) {

                if (player.score == 0) {
                    playerRank = 0;
                }
                return new ResponseEntity<>(playerRank, HttpStatus.OK);
            }

        }

    return new ResponseEntity<>(-1, HttpStatus.NOT_FOUND);
    }

    @GetMapping("/player/top")
    public ResponseEntity<List<Player>> getTop10Players() {
        List<Player> allPlayers = playerRepository.findAll();
        List<Player> top10 = new ArrayList<>();

        allPlayers.sort(Comparator.comparingInt(player -> player.score));

        // todo break out into function
        for (int i = 0; i < allPlayers.size(); i++) {

            if (i == 10) {
                break;
            }
            top10.add(allPlayers.get(i));
        }

        Collections.reverse(top10);

        return new ResponseEntity<>(top10, HttpStatus.OK);
    }

    @GetMapping("spotted/rarest")
    public ResponseEntity<List<GlobalSeenPokemon>> getTop10RarestPokemon() {

        List<GlobalSeenPokemon> allGlobalPokemon = globalSeenPokemonRepository.findAll();

        List<GlobalSeenPokemon> top10Rarest = new ArrayList<>();

        allGlobalPokemon.sort(Comparator.comparingInt(pokemon -> pokemon.seen));

        for (int i = 0; i < allGlobalPokemon.size(); i++) {

            if (i == 10) {
                break;
            }
            top10Rarest.add(allGlobalPokemon.get(i));
        }

        return new ResponseEntity<>(top10Rarest,HttpStatus.OK);
    }

    @PostMapping("/spotted")
    public ResponseEntity<String> addSpottedPokemon(@RequestBody ReceivedSpottedPokemon receivedSpottedPokemon) {

            Player player = playerRepository.getByUsername(receivedSpottedPokemon.caughtBy);
            short points = 0;

               Optional<GlobalSeenPokemon> pokemon = globalSeenPokemonRepository.findByPokemonId(Integer.parseInt(receivedSpottedPokemon.id));
               GlobalSeenPokemon globalPokemon = null;


               if(pokemon.isPresent()) {
                   globalPokemon = pokemon.get();
               } else {

                   /*
                   * If this is the first caught of the breed, a new "global" pokemon is created.
                   * */

                   globalPokemon = new GlobalSeenPokemon();
                   globalPokemon.pokemonId = Integer.parseInt(receivedSpottedPokemon.id);
                   globalPokemon.seen = 0;
               }

               /*
               *  "Else" or more accurately if in this case the pokemon has been spotted before and
               *   therefore exists we just report another spotted so the rarity ranking goes down.
               * */

               globalPokemon.seen += 1;
               globalSeenPokemonRepository.save(globalPokemon);

               for (SpottedPokemon oldPokemon: player.spottedPokemon) {

                   points += 10;

                   if (oldPokemon.pokemonId == Integer.parseInt(receivedSpottedPokemon.id)) {

                       oldPokemon.seen += 1;
                       oldPokemon.foundShiny = receivedSpottedPokemon.shiny;

                       if(oldPokemon.foundShiny) {
                           points += 50;
                       }

                       if (!oldPokemon.foundFemale && receivedSpottedPokemon.gender.equals("f")) {
                           oldPokemon.foundFemale = true;
                       }

                       if (!oldPokemon.foundMale && receivedSpottedPokemon.gender.equals("m")) {
                           oldPokemon.foundMale = true;
                       }
                        player.score += points;

                       playerRepository.save(player);
                       spottedPokemonRepository.save(oldPokemon);

                       return new ResponseEntity<>("Successfully updated pokemon", HttpStatus.OK);
                   }
               }

               // if the player does not have a pokemon this code will run

                SpottedPokemon newPokemon = new SpottedPokemon();

                points += 20;

                newPokemon.seen = 1;
                newPokemon.pokemonId = Integer.parseInt(receivedSpottedPokemon.id);
                newPokemon.player = player;

                if (receivedSpottedPokemon.gender.equals("m")) {
                    newPokemon.foundMale = true;
                    newPokemon.foundFemale = false;
                } else {
                    newPokemon.foundFemale = true;
                    newPokemon.foundMale = false;
                }


                newPokemon.foundShiny = receivedSpottedPokemon.shiny;

                if (newPokemon.foundShiny) {
                   points += 50;
                }

                newPokemon.firstFoundLatitude = receivedSpottedPokemon.latitude;
                newPokemon.firstFoundLongitude = receivedSpottedPokemon.longitude;

                newPokemon.firstFoundDate = LocalDateTime.now();

                player.score = points;

                spottedPokemonRepository.save(newPokemon);
                player.spottedPokemon.add(newPokemon);
                playerRepository.save(player);

                return new ResponseEntity<>("Successfully added spotted pokemon", HttpStatus.CREATED);
    }

}
